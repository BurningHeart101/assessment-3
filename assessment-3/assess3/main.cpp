#include "SFML/Graphics.hpp"
#include "Game.h"
#include <sstream>

int main()
{
	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "T012");

	//srand((unsigned int)time(0));

	//Initalise game objects
	Clock clock;
	float spawnTimer = 0.f;
	float spawnDelay = 0.01f;
	std::vector<Object> objects;
	std::vector<Object> bground;

	//Initialise background layers
	Texture backLayers[GC::MAX_LAYERS];
	for (int i = GC::MAX_LAYERS - 1; i >= 0; --i)
	{
		std::stringstream ss;
		ss << "data/mountains01_00" << i << ".png";
		LoadTexture(ss.str(), backLayers[i]);
		backLayers[i].setRepeated(true);

		Object background;
		background.Init(window, backLayers[i], Object::ObjT::Background);
		background.layerNum = i;	

		bground.push_back(background);
	}

	//Initialise ship
	Texture shipTex;
	LoadTexture("data/ship.png", shipTex);
	
	Object ship;
	ship.Init(window, shipTex, Object::ObjT::Ship);
	objects.push_back(ship);

	float rockShipClearance = objects[0].spr.getGlobalBounds().width * 2.f;

	//Initialise rocks
	Texture rockTex;
	LoadTexture("data/asteroids.png", rockTex);
	PlaceRocks(window, rockTex, objects);

	// Start the game loop 
	while (window.isOpen())
	{
		bool fire = false;
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed)
				window.close();
			if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close();
			}
		}

		// Clear screen
		window.clear();
		
		//Get time passed since last call
		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();

		spawnTimer += elapsed; //Add amount of time passed since last call
		if (spawnTimer >= spawnDelay) //If amount of time since last spawn is greater than spawnDelay
		{
			if (SpawnRock(window, objects, rockShipClearance)) //If you can spawn in a rock
				spawnTimer = 0; //Reset spawn timer to 0
		}

		//Check if any objects are colliding
		CheckCollisions(objects, window);

		//Update background
		for (size_t i = 0; i < bground.size(); ++i)
		{
			bground[i].Update(window, elapsed, objects);
			bground[i].Render(window);
		}

		//Update game objects
		for (size_t i = 0; i < objects.size(); ++i)
		{
			objects[i].Update(window, elapsed, objects);
			objects[i].Render(window);
		}

		
		// Update the window
		window.display();
	}
	return EXIT_SUCCESS;
}