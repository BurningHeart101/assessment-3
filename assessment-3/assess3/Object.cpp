#include "SFML/Graphics.hpp"
#include "Game.h"

#include <assert.h>
#include <string>
#include <math.h>

using namespace sf;

//Initialise game object
void Object::Init(RenderWindow& window, Texture& tex, ObjT type_)
{
	spr.setTexture(tex);
	switch (type_)
	{
	case ObjT::Ship:
		InitShip(window, tex);
		break;
	case ObjT::Rock:
		InitRock(window, tex);
		break;
	case ObjT::Background:
		InitBackground(window, tex);
		break;
	default:
		assert(false);
	}
}

//Initialise ship
void Object::InitShip(RenderWindow& window, Texture& tex)
{
	spr.setOrigin(tex.getSize().x / 2.f, tex.getSize().y / 2.f);
	spr.setScale(0.1f, 0.1f);
	spr.setRotation(90); //Make ship face right
	spr.setPosition(window.getSize().x * 0.05f, window.getSize().y / 2.f);
	radius = GC::SHIP_RADIUS;
	type = Object::ObjT::Ship;
	health = 3;
}

//Initialise rock
void Object::InitRock(RenderWindow& window, Texture& tex)
{
	IntRect texR{ 0, 0, 96, 96 }; //Crops image to first meteor sprite
	spr.setTextureRect(texR);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);

	radius = GC::ROCK_RAD.x + (float)(rand() % (int)GC::ROCK_RAD.y);
	float scale = 0.75f * (radius / 25.f); //Change rock scale based on radius of rock
	spr.setScale(scale, scale);
	spr.setRotation((float)(rand() % 360));
	type = Object::ObjT::Rock;
}

//Initialise background
void Object::InitBackground(RenderWindow& window, Texture& tex)
{
	spr.setOrigin(tex.getSize().x / 2.f, tex.getSize().y / 2.f);
	spr.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);
	spr.setScale((float)window.getSize().x / tex.getSize().x, (float)window.getSize().y / tex.getSize().y);
	type = Object::ObjT::Background;
	backgroundLayer = true;
}


//Update game object
void Object::Update(RenderWindow& window, float elapsed, std::vector<Object>& objects)
{
	if (active) //Only update objects if they are in use
	{
		colliding = false; 
		switch (type)
		{
		case ObjT::Ship:
			PlayerControl(window.getSize(), elapsed, objects);
			break;
		case ObjT::Rock:
			RotateRock();
			MoveRock(elapsed);
			break;
		case ObjT::Background:
			MoveBackground(elapsed);
			break;
		default:
			assert(false);
		}
	}
}

//Move player
void Object::PlayerControl(const Vector2u& screenSz, float elapsed, std::vector<Object>& objects)
{
	Vector2f pos = spr.getPosition();
	const float SPEED = 250.f;
	static Vector2f thrust{ 0, 0 };

	if (Keyboard::isKeyPressed(Keyboard::Up)) //Move up
	{
		if (pos.y > (screenSz.y * 0.05f))
			thrust.y = -SPEED;
	}
	else if (Keyboard::isKeyPressed(Keyboard::Down)) //Move down
	{
		if (pos.y < (screenSz.y * 0.95f))
			thrust.y = SPEED;
	}

	if (Keyboard::isKeyPressed(Keyboard::Left)) //Move left
	{
		if (pos.x > (screenSz.x * 0.05f))
			thrust.x = -SPEED;
	}
	else if (Keyboard::isKeyPressed(Keyboard::Right)) //Move right
	{
		if (pos.x < (screenSz.x * 0.95f))
			thrust.x = SPEED;
	}

	pos += thrust * elapsed; //Move player based on thrust
	thrust = Decay(thrust, 0.1f, 0.02f, elapsed); //Reduce thrust vector for smoother movement
	spr.setPosition(pos);
}

//Rotate rocks
void Object::RotateRock()
{
	float rot = spr.getRotation();
	rot -= 0.01f; //Rotate counter-clockwise
	spr.setRotation(rot);
}

//Move rocks
void Object::MoveRock(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::ROCK_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f) //If rock moves off left side of screen
		active = false;
	spr.setPosition(x, pos.y);
}

//Move background
void Object::MoveBackground(float elapsed)
{
	scrollOffset += GC::SCROLL_SPEED * elapsed * (GC::MAX_LAYERS - layerNum);
	IntRect rect{ 0 + (int)scrollOffset, 0, 512, 256 };
	spr.setTextureRect(rect);
}

//Register if two objects are hitting each other, make them take damage
void Object::Hit(Object& other)
{
	switch (type)
	{
	case ObjT::Ship:
		if (other.type == ObjT::Rock)
		{
			TakeDamage(1);
			other.TakeDamage(999); //Completely destroy rock
		}
		break;
	case ObjT::Rock:
		break;
	case ObjT::Background:
		break;
	default:
		assert(false);
	}
}

//Make the player take damage if hit by something
void Object::TakeDamage(int amount)
{
	health -= amount;
	if (health <= 0)
		active = false; //Is moved back into inactive list to be called in again
}

//Render game object
void Object::Render(RenderWindow& window)
{
	if (active)
		window.draw(spr);
}

//Spawn rocks during initialisation
void PlaceRocks(RenderWindow& window, Texture& tex, std::vector<Object>& objects)
{
	bool space = true;
	int ctr = GC::NUM_ROCKS;
	while (space && ctr) //While there is space and rocks to place
	{
		Object rock;
		rock.Init(window, tex, Object::ObjT::Rock);
		rock.radius *= GC::ROCK_MIN_DIST; //Radius = Radius * Minimum rock spacing distance.
		
		int tries = 0;
		do 
		{
			++tries;
			float x = (float)(rand() % window.getSize().x) + window.getSize().x; //Spawn rock on right offscreen
			float y = (float)(rand() % window.getSize().y);
			rock.spr.setPosition(x, y);
		} while (tries < GC::PLACE_TRIES && IsColliding(rock, objects));

		rock.radius *= 1 / GC::ROCK_MIN_DIST; //Reasssigns original radius to give space between rocks
		if (tries != GC::PLACE_TRIES) //If space was found
			objects.push_back(rock);
		else
			space = false; //If number of tries exceeds limit, says there is no more room and thus function ends
		--ctr;
	}
}

//Load texture for game object
bool LoadTexture(const std::string& file, Texture& tex)
{
	if (tex.loadFromFile(file))
	{
		tex.setSmooth(true);
		return true;
	}
	assert(false);
	return false;
}

//Create collision circle
void DrawCircle(RenderWindow& window, const Vector2f& pos, float radius, Color col)
{
	CircleShape c;
	c.setRadius(radius); //Circle radius = collision radius
	c.setPointCount(20);
	c.setOutlineColor(col);
	c.setOutlineThickness(2);
	c.setFillColor(Color::Transparent);
	c.setPosition(pos);
	c.setOrigin(radius, radius); //Circle origin = centre of object
	window.draw(c);
}

//Check if two game objects are colliding
void CheckCollisions(std::vector<Object>& objects, RenderWindow& window, bool debug)
{
	if (objects.size() > 1)
	{
		for (size_t i = 0; i < objects.size(); ++i)
		{
			Object& a = objects[i]; //First object
			if (a.active)
			{
				if (i < (objects.size() - 1)) //Final object in vector will already have checked against every other object, so not needed
					for (size_t j = i + 1; j < (objects.size()); ++j) //For every object not checked against current object
					{
						Object& b = objects[j]; //Second object
						if (b.active)
						{
							if (CircleToCircle(a.spr.getPosition(), b.spr.getPosition(), a.radius + b.radius)) //If intersecting
							{
								a.colliding = true;
								b.colliding = true;
								a.Hit(b);  //Make object A take damage
								b.Hit(a); //Make object B take damage
							}
						}
					}
				if (debug) //If in debug mode
				{
					Color col = Color::Green;
					if (a.colliding)
						col = Color::Red;
					DrawCircle(window, a.spr.getPosition(), a.radius, col);
				}

			}
		}
	}
}

//Checks to see if objects are intersecting using pythagorus.
bool CircleToCircle(const Vector2f& pos1, const Vector2f& pos2, float minDist)
{
	float dist = (pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y); //a^2 + b^2 = c^2
	dist = sqrtf(dist);
	return dist <= minDist;
}

//Calculates position of objects and then returns true if they intersect
bool IsColliding(Object& obj, std::vector<Object>& objects)
{
	size_t idx = 0;
	bool colliding = false;

	//Check every object apart from current one for collisions
		while (idx < objects.size() && !colliding)
		{
			if (&obj != &objects[idx] && objects[idx].type == Object::ObjT::Rock)
			{
				const Vector2f& posA = obj.spr.getPosition();
				const Vector2f& posB = objects[idx].spr.getPosition();
				float dist = obj.radius + objects[idx].radius;
				colliding = CircleToCircle(posA, posB, dist); //See if collision intersects
			}
			++idx;
		}
	return colliding;
}

//Spawn in rocks when they move offscreen
bool SpawnRock(RenderWindow& window, std::vector<Object>& objects, float extraClearance)
{
	//find a !active rock we can use again
	size_t idx = 0;
	bool found = false;
	while (idx < objects.size() && !found)
	{
		Object& obj = objects[idx];
		if (!obj.active && obj.type == Object::ObjT::Rock) //If object is an inactive rock
			found = true;
		else
			++idx;
	}

	if (found)
	{
		//place it just off screen
		Object& obj = objects[idx];
		obj.active = true;
		obj.radius += extraClearance; //Increase radius of rocks
		FloatRect r = obj.spr.getGlobalBounds();
		float y = (r.height / 2.f);
		y += (rand() % (int)(window.getSize().y - r.height));
		obj.spr.setPosition(window.getSize().x + r.width, y);
		if (IsColliding(obj, objects))
		{
			//if it is colliding then ignore it
			found = false;
			obj.active = false;
		}
		obj.radius -= extraClearance; //Set radius to original value s
	}
	return found;
}

/*
reduce a vector by a certain percentage over a certain time
currentVel - the vector you need to make a reduced version of
pcnt - at the end of timeInterval it should have reduced by this percentage (e.g. 10%=0.1)
timeInterval - how many seconds for the pcnt reduction to complete
dTimeS - actual elapsed time e.g. 10% after 1sec, so if dTimeS=0.1s, then the reduction will be 1%
*/
Vector2f Decay(Vector2f& currentVal, float pcnt, float timeInterval, float dTimeS)
{
	float mod = 1.0f - pcnt * (dTimeS / timeInterval);
	Vector2f alpha(currentVal.x * mod, currentVal.y * mod);
	return alpha;
}